#!/bin/bash
set -e

echo "Building 'poetry deploy' stage..."
echo "'poetry deploy' stage is built"

echo "Building 'poetry test' stage..."
echo "'poetry test' stage is built"

if [ "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ]; then
  echo "Building final 'deploy' stage..."
  echo "Final 'deploy' stage is built"
fi

echo "Building final 'test' stage..."
echo "Final 'test' stage is built"
