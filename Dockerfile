FROM python:3.11.5-alpine3.18

WORKDIR /pysnail

COPY . .

WORKDIR /pysnail/src

CMD ["python3", "-m", "pysnail"]
